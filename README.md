<img alt="Quality Gate Status" src="https://sonarcloud.io/api/project_badges/measure?project=springfast&metric=alert_status"/>

<img alt="Maintainability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=springfast&metric=sqale_rating"/>

<img alt="Reliability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=springfast&metric=reliability_rating"/>

<img alt="Security Rating" src="https://sonarcloud.io/api/project_badges/measure?project=springfast&metric=security_rating"/>

<img alt="Bugs" src="https://sonarcloud.io/api/project_badges/measure?project=springfast&metric=bugs"/>

<img alt="Vulnerabilities" src="https://sonarcloud.io/api/project_badges/measure?project=springfast&metric=vulnerabilities"/>

<img alt="Pipeline" src="https://img.shields.io/gitlab/pipeline/fsorge/springfast/main"/>


# SpringFast

Automatically creates controllers, DAOs, DTOs, converters, services and repositories


## Install

Install SpringFast globally on your system:
```bash
npm i -g springfast
```

## Project initialization

1. Navigate to the folder where you want your project to be generated (a folder with the project name will be automatically created)

2. Execute
```bash
springfast
```

3. Follow the onscreen instructions


## Project configuration and generation of classes

SpringFast can configure your application.yml with the server port and database connection parameters. It can also generate DAOs, DTOs, controllers, services and repositories.

1. Navigate to the project folder

2. Execute
```bash
springfast
```

3. SpringFast will automatically recognize that it's a Spring project and you will have to follow the onscreen instructions

## Develop

Build and run on Windows: `npm run build; npm i -g .; springfast`

Build and run on Linux: `npm run build && npm i -g . && springfast`