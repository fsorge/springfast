import fs = require('fs');
import path = require('path');
import parser = require('fast-xml-parser');
import prompts = require('prompts');
import table = require('cli-table');
import { PropertiesToYml } from 'properties-to-yml';

import {
  convertToJavaVariableConvention,
  convertToJavaClassConvention,
  exitOnAbort,
} from './common';
import IPom from './interfaces/IPom';
import IField from './interfaces/IField';

// Path to the project
let p: string;
// Will contain pom in JSON format if project is a Maven one
let pom: IPom;
// Path to the resources folder of the project
let resourcesPath: string;

/**
 * Launches the project manager
 * @param projectPath path to the project
 */
export const project = async (projectPath: string) => {
  p = projectPath;
  resourcesPath = path.join(p, 'src', 'main', 'resources');

  /*
  If project contains a pom.xml file (in other words: if it's a Maven project)
  */
  if (fs.existsSync(path.join(p, 'pom.xml'))) {
    // We parse the pom file into a JSON
    pom = parser.parse(fs.readFileSync(path.join(p, 'pom.xml'), 'utf8'));
  }

  // If the project is a Maven one
  if (pom) {
    // We can print the project name
    console.log(`Project ${pom.project.name}\n`);
  } else {
    // TODO We should handle a Gradle project here
  }

  let exitMenu = false;
  do {
    const { menuSelection } = await prompts({
      type: 'select',
      name: 'menuSelection',
      message: 'What would you like to do?',
      onState: exitOnAbort,
      choices: [
        { title: '❌ Exit', value: 'exit' },
        // { title: '🟢 Start server', value: 'start-server' },
        { title: '📃 List dependencies', value: 'list-deps' },
        {
          title: '✏ Configuration (application.yml/properties)',
          value: 'configuration',
        },
        { title: '🚗 Generate entity', value: 'generate-entity' },
      ],
    });

    exitMenu = menuSelection === 'exit';

    if (!exitMenu) {
      switch (menuSelection) {
        case 'list-deps':
          listDeps();
          break;
        case 'configuration':
          await configuration();
          break;
        case 'generate-entity':
          await generateEntity();
          break;
      }
    }
  } while (!exitMenu);
};

/**
 * Prints to a table all project dependencies (it currently supports only Maven)
 */
const listDeps = () => {
  // If pom is not parsed into JSON (if it's not a Maven project)
  if (!pom) {
    // We mark this feature as not available
    // TODO we should handle Gradle projects
    console.warn('This feature is currently available only for Maven projects');
    return;
  }

  // We create table's header
  const t = new table({
    head: ['Group ID', 'Artifact ID', 'mvn repository'],
  });
  // We push rows
  t.push(
    ...pom.project.dependencies.dependency.map((d) => [
      d.groupId,
      d.artifactId,
      `https://mvnrepository.com/artifact/${d.groupId}/${d.artifactId}`,
    ]),
  );
  // Finally, we print it
  console.log(t.toString());
};

/**
 * Launches project main configuration wizard (it does not support profiles)
 */
const configuration = async () => {
  // Path to main application.properties
  const appProperties = path.join(resourcesPath, 'application.properties');
  // Path to main application.yml
  const appYml = path.join(resourcesPath, 'application.yml');

  // If an application.properties file exists
  if (fs.existsSync(appProperties)) {
    console.log(
      'SpringFast can only manage yml files.\nI can convert your application.proper' +
        "ties to application.yml and won't delete your application.properties, but " +
        'rename it to application.properties.bak',
    );
    // It asks to convert it to a yml file
    const { convert } = await prompts({
      type: 'confirm',
      name: 'convert',
      initial: false,
      message: `Can I convert it?`,
    });

    // If the user refuses
    if (!convert) {
      // We stop the configuration wizard
      console.warn("Then I can't manage your application.properties.");
      return;
    }

    try {
      // Converts application.properties to application.yml
      new PropertiesToYml().fromFile(appProperties).convert().toFile(appYml);
    } catch (e) {
      // If an error is triggered, we simply write an empty application.yml
      fs.writeFileSync(appYml, '');
    }

    /**
     * For security reasons, we do not delete good old application.properties,
     * we rename it to application.properties.bak
     */
    fs.renameSync(appProperties, `${appProperties}.bak`);
  }

  /**
   * If we still can't find a yml file, it means something wrong happened and
   * we exit the configuration
   */
  if (!fs.existsSync(appYml)) {
    console.error("Can't find the application.yml file");
    return;
  }

  let exitMenu = false;
  let configStruct;
  do {
    const { configurationMenu } = await prompts({
      type: 'select',
      name: 'configurationMenu',
      message: 'What would you like to configure?',
      onState: exitOnAbort,
      choices: [
        { title: 'Exit', value: 'exit' },
        { title: 'Server generic (port)', value: 'generic' },
        { title: 'Database', value: 'database' },
      ],
    });

    exitMenu = configurationMenu === 'exit';

    if (!exitMenu) {
      // We dynamically load yaml parser library
      const yaml = await import('js-yaml');

      // If config struct is empty, it means we did not load it yet
      if (!configStruct) {
        // So we load it
        configStruct = yaml.load(fs.readFileSync(appYml, 'utf8'));
      }

      switch (configurationMenu) {
        case 'generic':
          configStruct = await genericConfig(configStruct);
          break;
        case 'database':
          configStruct = await databaseConfig(configStruct);
          break;
      }

      // Finally, we write the configuration to file
      fs.writeFileSync(appYml, yaml.dump(configStruct));
    }
  } while (!exitMenu);
};

/**
 * Asks for project generic configurations (server port)
 * @param configStruct JSON corrispondent YML configuration
 * @returns a promise containing the new JSON configuration
 */
const genericConfig = async (configStruct: any): Promise<any> => {
  // We initialize fields if they're missing
  if (!configStruct) {
    configStruct = {};
  }

  if (!configStruct.server) {
    configStruct.server = {};
  }

  const { serverPort } = await prompts({
    type: 'number',
    name: 'serverPort',
    message: 'Server port:',
    initial: configStruct.server.port ?? 8080,
    onState: exitOnAbort,
  });

  configStruct.server.port = serverPort;

  return configStruct;
};

/**
 * We initialize database configuration fields if they're missing
 * @param configStruct JSON corrispondent YML configuration
 * @returns initialized `configStruct`
 */
const initializeDatabaseConfig = (configStruct: any): any => {
  if (!configStruct) {
    configStruct = {};
  }

  if (!configStruct.spring) {
    configStruct.spring = {};
  }

  if (!configStruct.spring.datasource) {
    configStruct.spring.datasource = {};
  }

  if (!configStruct.spring.jpa) {
    configStruct.spring.jpa = {};
  }

  if (!configStruct.spring.jpa.properties) {
    configStruct.spring.jpa.properties = {};
  }

  if (!configStruct.spring.jpa.properties.hibernate) {
    configStruct.spring.jpa.properties.hibernate = {};
  }
  return configStruct;
};

/**
 * Asks for database configuration, DDL generation and queries debug
 * @param configStruct JSON corrispondent YML configuration
 * @returns a promise containing the new JSON configuration
 */
const databaseConfig = async (configStruct: any): Promise<any> => {
  configStruct = initializeDatabaseConfig(configStruct);

  const { host } = await prompts({
    type: 'text',
    name: 'host',
    message: 'Host address:',
    onState: exitOnAbort,
    validate: (v: string) => {
      if (/\s/g.test(v)) {
        return 'Host address cannot contain white spaces';
      } else {
        return true;
      }
    },
  });

  const { port } = await prompts({
    type: 'number',
    name: 'port',
    message: 'Port:',
    onState: exitOnAbort,
  });

  const { databaseName } = await prompts({
    type: 'text',
    name: 'databaseName',
    message: 'Database name:',
    onState: exitOnAbort,
    validate: (v: string) => {
      if (/\s/g.test(v)) {
        return 'Database name cannot contain white spaces';
      } else {
        return true;
      }
    },
  });

  const { username } = await prompts({
    type: 'text',
    name: 'username',
    message: 'Username:',
    initial: configStruct.spring.datasource.username ?? '',
    onState: exitOnAbort,
    validate: (v: string) => {
      if (/\s/g.test(v)) {
        return 'Username cannot contain white spaces';
      } else {
        return true;
      }
    },
  });

  const { password } = await prompts({
    type: 'text',
    name: 'password',
    message: 'Password:',
    initial: configStruct.spring.datasource.password ?? '',
    onState: exitOnAbort,
  });

  // All supported databases
  const supportedDatabases: prompts.Choice[] = [
    // https://www.javatpoint.com/dialects-in-hibernate
    {
      title: 'Postgres/PostgreSQL',
      value: {
        dialect: 'org.hibernate.dialect.PostgreSQLDialect',
        connectionString: `jdbc:postgresql://${host}:${port}/${databaseName}`,
      },
    },
    {
      title: 'MySQL',
      value: {
        dialect: 'org.hibernate.dialect.MySQLDialect',
        connectionString: `jdbc:mysql://${host}:${port}/${databaseName}`,
      },
    },
    {
      title: 'MariaDB',
      value: {
        dialect: 'org.hibernate.dialect.MariaDBDialect',
        connectionString: `jdbc:mariadb://${host}:${port}/${databaseName}`,
      },
    },
    {
      title: 'SQL Server',
      value: {
        dialect: 'org.hibernate.dialect.SQLServerDialect',
        connectionString: `jdbc:sqlserver://${host}:${port};databaseName=${databaseName}`,
      },
    },
    {
      title: 'Oracle',
      value: {
        dialect: 'org.hibernate.dialect.OracleDialect',
        connectionString: `jdbc:oracle:thin:@${host}:${port}:${databaseName}`,
      },
    },
  ];

  // Tries to read current dialect from current configuration
  let supportedDatabaseIndex = supportedDatabases.findIndex(
    (d) => d.value === configStruct.spring.jpa.properties.hibernate.dialect,
  );
  // If it can't find one
  if (supportedDatabaseIndex < 0) {
    // It sets the default choice to the first oen
    supportedDatabaseIndex = 0;
  }

  const { database } = await prompts({
    type: 'select',
    name: 'database',
    message: 'Which database engine are you using?',
    initial: supportedDatabaseIndex,
    onState: exitOnAbort,
    choices: supportedDatabases,
  });

  const { showLogs } = await prompts({
    type: 'confirm',
    name: 'showLogs',
    initial: configStruct.spring.jpa['show-sql'] ?? false,
    message: `Would you like to print SQL queries to console?`,
    onState: exitOnAbort,
  });

  const { autoDDL } = await prompts({
    type: 'confirm',
    name: 'autoDDL',
    initial: configStruct.spring.jpa['generate-ddl'] ?? true,
    message: `Would you like to auto generate DDL (eg: tables)?`,
    onState: exitOnAbort,
  });

  configStruct.spring.datasource.url = database.connectionString;
  configStruct.spring.datasource.username = username;
  configStruct.spring.datasource.password = password;
  configStruct.spring.jpa.properties.hibernate.dialect = database.dialect;
  configStruct.spring.jpa['show-sql'] = showLogs;
  configStruct.spring.jpa['generate-ddl'] = autoDDL;
  configStruct.spring.jpa.properties.hibernate['ddl-auto'] = autoDDL
    ? 'create'
    : 'none';

  return configStruct;
};

/**
 * Generates a new entity, dto, repository, service and controller
 */
const generateEntity = async () => {
  // Picks path to main java
  const mainJava = path.join(p, 'src', 'main', 'java');

  // If pom is not parsed into JSON (if it's not a Maven project)
  if (!pom) {
    // We mark this feature as not available
    // TODO we should handle Gradle projects
    console.warn('This feature is currently available only for Maven projects');
    return;
  }

  // Reads pom groupId to get package as array
  const packageAsArray = [
    ...pom.project.groupId.split('.'),
    pom.project.artifactId,
  ];

  const { name, tableName, fields } = await askEntityBasics();
  await generateDao(mainJava, packageAsArray, name, fields, tableName);
  await generateDto(mainJava, packageAsArray, name, fields);
  await generateRepository(mainJava, packageAsArray, name, fields);
  await generateService(mainJava, packageAsArray, name, fields);
  await generateController(mainJava, packageAsArray, name, fields);
};

/**
 * Asks for entity name, table name and a list of fields
 * @returns a JSON containing entity name, table name and a list of fields
 */
const askEntityBasics = async (): Promise<{
  name: string;
  tableName: string;
  fields: IField[];
}> => {
  const { name } = await prompts({
    type: 'text',
    name: 'name',
    message: 'Choose entity class name:',
    onState: exitOnAbort,
    validate: (s: string) => {
      const validate = s.trim().length > 0;
      return validate ? validate : 'Class name cannot be empty';
    },
    format: convertToJavaClassConvention,
  });

  const { tableName } = await prompts({
    type: 'text',
    name: 'tableName',
    message: 'Table name:',
    initial: name.toLowerCase(),
    onState: exitOnAbort,
    validate: (s: string) => {
      const validate = s.trim().length > 0;
      return validate ? validate : 'Table name cannot be empty';
    },
    format: (s: string) => {
      return s.toLowerCase();
    },
  });

  const fields = await askForFields();

  return {
    name,
    tableName,
    fields,
  };
};

const askForFields = async (): Promise<IField[]> => {
  const fields: IField[] = [];

  let accept = false;
  do {
    const { fieldName } = await prompts({
      type: 'text',
      name: 'fieldName',
      message: 'Field name:',
      onState: exitOnAbort,
      validate: (s: string) => {
        const validate = s.trim().length > 0;
        return validate ? validate : 'Field name cannot be empty';
      },
    });

    const { columnName } = await prompts({
      type: 'text',
      name: 'columnName',
      message: 'Column name:',
      initial: fieldName,
      onState: exitOnAbort,
      validate: (s: string) => {
        const validate = s.trim().length > 0;
        return validate ? validate : 'Field name cannot be empty';
      },
    });

    const { isNullable } = await prompts({
      type: 'confirm',
      name: 'isNullable',
      message: 'Is nullable?',
      onState: exitOnAbort,
    });

    const { isPK } = await prompts({
      type: 'confirm',
      name: 'isPK',
      message: 'Is primary key?',
      onState: exitOnAbort,
    });

    const { fieldType } = await prompts({
      type: 'select',
      name: 'fieldType',
      message: 'Field type:',
      onState: exitOnAbort,
      choices: [
        { title: 'String', value: 'String' },
        { title: 'Integer', value: 'Integer' },
        { title: 'Long', value: 'Long' },
        { title: 'Float', value: 'Float' },
        { title: 'Double', value: 'Double' },
        { title: 'Boolean', value: 'Boolean' },
        { title: 'Date', value: 'Date' },
      ],
    });

    let hasGeneratedValue = false;
    if ((fieldType === 'Integer' || fieldType === 'Long') && isPK) {
      hasGeneratedValue = (
        await prompts({
          type: 'confirm',
          name: 'hasGeneratedValue',
          message: 'Should it be automatically generated?',
          onState: exitOnAbort,
        })
      ).hasGeneratedValue;
    }

    fields.push({
      name: fieldName,
      columnName,
      type: fieldType,
      isNullable,
      isPK,
      hasGeneratedValue,
    });

    printFieldsTable(fields);

    const { another } = await prompts({
      type: 'confirm',
      name: 'another',
      message: 'Would you like to add another field?',
      onState: exitOnAbort,
    });

    accept = !another;
  } while (!accept);

  return fields;
};

/**
 * Prints a table showing all fields name and attributes (PK, NULL, A_I, etc)
 * @param fields entity fields
 */
const printFieldsTable = (fields: IField[]) => {
  const t = new table({
    head: ['Is PK?', 'Name', 'Type', 'Is nullable?', 'Auto generate?'],
  });
  t.push(
    ...fields.map((f) => [
      f.isPK ? 'Yes' : 'No',
      f.name,
      f.type,
      f.isNullable ? 'Yes' : 'No',
      f.hasGeneratedValue ? 'Yes' : 'No',
    ]),
  );
  console.log(t.toString());
};

/**
 * Generates DAO class
 * @param mainJava path to main java folder
 * @param packageAsArray project main package as an array
 * @param name name of the entity
 * @param fields list of fields
 * @param tableName name of the table
 */
const generateDao = async (
  mainJava: string,
  packageAsArray: string[],
  name: string,
  fields: IField[],
  tableName: string,
): Promise<any> => {
  const daoPath = path.join(mainJava, ...packageAsArray, 'dao');

  // If the folder does not exist, we create it
  if (!fs.existsSync(daoPath)) {
    fs.mkdirSync(daoPath);
  }

  // Array of imports
  const imports = [
    'javax.persistence.Basic',
    'javax.persistence.Column',
    'javax.persistence.Entity',
    'javax.persistence.Id',
    'javax.persistence.Table',
    'javax.persistence.GeneratedValue',
    'java.util.Date',
  ];

  // We write package, imports, class annotations and class signature
  let content = `package ${[...packageAsArray, 'dao'].join('.')};\n\n${imports
    .map((i) => `import ${i};\n`)
    .join(
      '',
    )}\n@Entity\n@Table(name = "${tableName}")\npublic class ${name} {\n`;

  // For each field
  for (const field of fields) {
    // If it is a Primary Key
    if (field.isPK) {
      // We add the @Id annotation
      content += `\t@Id\n`;
      // If its value should be autogenerated
      if (field.hasGeneratedValue) {
        // We add the @GeneratedValue annotation
        content += `\t@GeneratedValue\n`;
      }
    }

    content += `\t@Column(name = "${field.columnName}")\n`;
    content += `\t@Basic(optional = ${field.isNullable ? 'true' : 'false'})\n`;
    content += `\tprivate ${field.type} ${field.name};\n\n`;
  }

  // And for each field we build its getter and fluent setter
  for (const field of fields) {
    content += `\tpublic ${field.type} get${convertToJavaClassConvention(
      field.name,
    )}() {\n\t\treturn this.${field.name};\n\t}\n`;
    content += `\tpublic ${name} set${convertToJavaClassConvention(
      field.name,
    )}(${field.type} ${field.name}) {\n\t\tthis.${field.name} = ${
      field.name
    };\n\t\treturn this;\n\t}\n\n`;
  }

  content += `}`;

  // Finally, we write the Java class to disk
  fs.writeFileSync(path.join(daoPath, `${name}.java`), content);
};

/**
 * Generates DTO class
 * @param mainJava path to main java folder
 * @param packageAsArray project main package as an array
 * @param name name of the entity
 * @param fields list of fields
 */
const generateDto = async (
  mainJava: string,
  packageAsArray: string[],
  name: string,
  fields: IField[],
): Promise<any> => {
  const dtoPath = path.join(mainJava, ...packageAsArray, 'dto');

  // If the folder does not exist, we create it
  if (!fs.existsSync(dtoPath)) {
    fs.mkdirSync(dtoPath);
  }

  // Array of imports
  const imports = ['java.util.Date'];

  // We write package, imports, class annotations and class signature
  let content = `package ${[...packageAsArray, 'dto'].join('.')};\n\n${imports
    .map((i) => `import ${i};\n`)
    .join('')}\n\npublic class ${name}Dto {\n`;

  // For each field, we mark it as private
  for (const field of fields) {
    content += `\tprivate ${field.type} ${field.name};\n\n`;
  }

  // And for each field we build its getter and fluent setter
  for (const field of fields) {
    content += `\tpublic ${field.type} get${convertToJavaClassConvention(
      field.name,
    )}() {\n\t\treturn this.${field.name};\n\t}\n`;
    content += `\tpublic ${name}Dto set${convertToJavaClassConvention(
      field.name,
    )}(${field.type} ${field.name}) {\n\t\tthis.${field.name} = ${
      field.name
    };\n\t\treturn this;\n\t}\n\n`;
  }

  content += `}`;

  // Finally, we write the Java class to disk
  fs.writeFileSync(path.join(dtoPath, `${name}Dto.java`), content);
};

/**
 * Generates Repository class
 * @param mainJava path to main java folder
 * @param packageAsArray project main package as an array
 * @param name name of the entity
 * @param fields list of fields
 */
const generateRepository = async (
  mainJava: string,
  packageAsArray: string[],
  name: string,
  fields: IField[],
): Promise<any> => {
  const repositoryPath = path.join(mainJava, ...packageAsArray, 'repository');

  // If the folder does not exist, we create it
  if (!fs.existsSync(repositoryPath)) {
    fs.mkdirSync(repositoryPath);
  }

  // Array of imports
  const imports = [
    'org.springframework.data.jpa.repository.JpaRepository',
    'org.springframework.stereotype.Repository',
    `${[...packageAsArray, 'dao'].join('.')}.${name}`,
  ];

  /**
   * We write package, imports, class annotations and class signature
   * We must also find the primary key and write down its type
   */
  const content = `package ${[...packageAsArray, 'repository'].join(
    '.',
  )};\n\n${imports
    .map((i) => `import ${i};\n`)
    .join(
      '',
    )}\n@Repository\npublic interface ${name}Repository extends JpaRepository<${name}, ${
    fields.find((f) => f.isPK)?.type
  }> { }`;

  // Finally, we write the Java class to disk
  fs.writeFileSync(
    path.join(repositoryPath, `${name}Repository.java`),
    content,
  );
};

/**
 * Generates Service class
 * @param mainJava path to main java folder
 * @param packageAsArray project main package as an array
 * @param name name of the entity
 * @param fields list of fields
 */
const generateService = async (
  mainJava: string,
  packageAsArray: string[],
  name: string,
  fields: IField[],
): Promise<any> => {
  const servicePath = path.join(mainJava, ...packageAsArray, 'service');

  // If the folder does not exist, we create it
  if (!fs.existsSync(servicePath)) {
    fs.mkdirSync(servicePath);
  }

  // Array of imports
  const imports = [
    'java.util.Optional',
    'java.util.List',
    'java.util.ArrayList',
    'javax.transaction.Transactional',
    'org.springframework.beans.factory.annotation.Autowired',
    'org.springframework.stereotype.Service',
    `${[...packageAsArray, 'dao'].join('.')}.${name}`,
    `${[...packageAsArray, 'dto'].join('.')}.${name}Dto`,
    `${[...packageAsArray, 'repository'].join('.')}.${name}Repository`,
  ];

  const repositoryVariableName = `${convertToJavaVariableConvention(
    name,
  )}Repository`;

  // Finds entity primary key
  const primaryKey = fields.find((f) => f.isPK);

  // If primary key is missing
  if (!primaryKey) {
    // We abort the process since it's required
    console.error(
      "This entity does not have a primary key but it's required. Aborting.",
    );
    return;
  }

  // We write package, imports, class annotations and class signature
  let content = `package ${[...packageAsArray, 'service'].join(
    '.',
  )};\n\n${imports
    .map((i) => `import ${i};\n`)
    .join(
      '',
    )}\n@Service\n@Transactional\npublic class ${name}Service {\n\n\t@Autowired\n\tprivate ${name}Repository ${repositoryVariableName};\n\n`;

  // daoToDto method
  content += `\tprivate ${name}Dto daoToDto(${name} dao) {\n\t\t${name}Dto dto = new ${name}Dto();\n`;
  for (const field of fields) {
    content += `\t\tdto.set${convertToJavaClassConvention(
      field.name,
    )}(dao.get${convertToJavaClassConvention(field.name)}());\n`;
  }
  content += `\t\treturn dto;\n\t}\n\n`;

  // dtoToDao method
  content += `\tprivate ${name} dtoToDao(${name}Dto dto) {\n\t\t${name} dao = new ${name}();\n`;
  for (const field of fields) {
    content += `\t\tdao.set${convertToJavaClassConvention(
      field.name,
    )}(dto.get${convertToJavaClassConvention(field.name)}());\n`;
  }
  content += `\t\treturn dao;\n\t}\n\n`;

  // findAll method
  content += `\tpublic List<${name}Dto> findAll() {\n\t\tList<${name}Dto> dtos = new ArrayList<>();\n\t\tfor (${name} dao : ${repositoryVariableName}.findAll()) {\n\t\t\tdtos.add(this.daoToDto(dao));\n\t\t}\n\t\treturn dtos;\n\t}\n\n`;

  // findById method
  content += `\tpublic Optional<${name}Dto> findById(${primaryKey.type} id) {\n\t\tOptional<${name}> dao = ${repositoryVariableName}.findById(id); \n\t\tif (dao.isPresent()) {\n\t\t\treturn Optional.of(this.daoToDto(dao.get()));\n\t\t}\n\t\treturn Optional.empty();\n\t}\n\n`;

  // insert method
  content += `\tpublic void insert(${name}Dto dto) {\n\t\t${repositoryVariableName}.saveAndFlush(this.dtoToDao(dto));\n\t}\n\n`;

  // update method
  content += `\tpublic boolean update(${name}Dto dto) {\n\t\tif (${repositoryVariableName}.findById(dto.get${convertToJavaClassConvention(
    primaryKey.name,
  )}()).isPresent()) { \n\t\t\t${repositoryVariableName}.save(this.dtoToDao(dto));\n\t\t\treturn true;\n\t\t}\n\t\treturn false;\n\t}\n\n`;

  // delete method
  content += `\tpublic boolean deleteById(${primaryKey?.type} id) {\n\t\tif (${repositoryVariableName}.findById(id).isPresent()) { \n\t\t\t${repositoryVariableName}.deleteById(id);\n\t\t\treturn true;\n\t\t}\n\t\treturn false;\n\t}\n\n`;

  content += '}';

  // Finally, we write the Java class to disk
  fs.writeFileSync(path.join(servicePath, `${name}Service.java`), content);
};

/**
 * Generates Controller class
 * @param mainJava path to main java folder
 * @param packageAsArray project main package as an array
 * @param name name of the entity
 * @param fields list of fields
 */
const generateController = async (
  mainJava: string,
  packageAsArray: string[],
  name: string,
  fields: IField[],
): Promise<any> => {
  const controllerPath = path.join(mainJava, ...packageAsArray, 'controller');

  // If the folder does not exist, we create it
  if (!fs.existsSync(controllerPath)) {
    fs.mkdirSync(controllerPath);
  }

  // Array of imports
  const imports = [
    'java.util.Optional',
    'java.util.List',
    'java.util.ArrayList',
    'org.springframework.beans.factory.annotation.Autowired',
    'org.springframework.web.bind.annotation.RestController',
    'org.springframework.web.bind.annotation.RequestMapping',
    'org.springframework.web.bind.annotation.GetMapping',
    'org.springframework.web.bind.annotation.PostMapping',
    'org.springframework.web.bind.annotation.PutMapping',
    'org.springframework.web.bind.annotation.DeleteMapping',
    'org.springframework.web.bind.annotation.PathVariable',
    'org.springframework.web.bind.annotation.RequestBody',
    `${[...packageAsArray, 'dto'].join('.')}.${name}Dto`,
    `${[...packageAsArray, 'service'].join('.')}.${name}Service`,
  ];

  const serviceVariableName = `${convertToJavaVariableConvention(name)}Service`;

  // Finds entity primary key
  const primaryKey = fields.find((f) => f.isPK);

  // We write package, imports, class annotations and class signature
  let content = `package ${[...packageAsArray, 'controller'].join(
    '.',
  )};\n\n${imports
    .map((i) => `import ${i};\n`)
    .join(
      '',
    )}\n@RestController\n@RequestMapping(value = "api/${convertToJavaVariableConvention(
    name,
  )}")\npublic class ${name}Controller {\n\n\t@Autowired\n\tprivate ${name}Service ${serviceVariableName};\n\n`;

  // findAll method
  content += `\t@GetMapping(produces = "application/json")\n\tpublic List<${name}Dto> findAll() {\n\t\treturn ${serviceVariableName}.findAll();\n\t}\n\n`;

  // findById method
  content += `\t@GetMapping(value = "{id}", produces = "application/json")\n\tpublic Optional<${name}Dto> findById(@PathVariable("id") ${primaryKey?.type} id) {\n\t\treturn ${serviceVariableName}.findById(id);\n\t}\n\n`;

  // insert method
  content += `\t@PostMapping(produces = "application/json")\n\tpublic boolean insert(@RequestBody() ${name}Dto dto) {\n\t\t${serviceVariableName}.insert(dto);\n\t\treturn true;\n\t}\n\n`;

  // update method
  content += `\t@PutMapping(produces = "application/json")\n\tpublic boolean update(@RequestBody() ${name}Dto dto) {\n\t\treturn ${serviceVariableName}.update(dto);\n\t}\n\n`;

  // delete method
  content += `\t@DeleteMapping(value = "{id}", produces = "application/json")\n\tpublic boolean deleteById(@PathVariable("id") ${primaryKey?.type} id) {\n\t\treturn ${serviceVariableName}.deleteById(id);\n\t}\n\n`;

  content += '}';

  // Finally, we write the Java class to disk
  fs.writeFileSync(
    path.join(controllerPath, `${name}Controller.java`),
    content,
  );
};
