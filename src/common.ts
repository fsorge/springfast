import { exec } from 'child_process';

import { SpringCLIConfig } from './config/SpringCLI';

export const exitOnAbort = (state: any) => {
  if (state.aborted) {
    process.nextTick(() => {
      process.exit(0);
    });
  }
};

export const escapeExec = (s: string) => {
  return s.replace(/(["'$`\\])/g, '\\$1');
};

export const convertToJavaVariableConvention = (s: string) => {
  return s[0].toLowerCase() + (s.length > 1 ? s.substr(1) : '');
};

export const convertToJavaClassConvention = (s: string) => {
  return s[0].toUpperCase() + (s.length > 1 ? s.substr(1) : '');
};

export const setSpringCliCmd = (s: string) => {
  SpringCLIConfig.springCliPath = s;
};

export const execSpring = (command: string): Promise<string> => {
  return execAsync(SpringCLIConfig.springCliPath, command);
};

export const execAsync = (
  executable: string,
  parameters: string,
): Promise<string> => {
  return new Promise((resolve, reject) => {
    exec(
      `${executable} ${parameters}`,
      (error: any, stdout: string, stderr: string) => {
        if (error) {
          reject(error);
        }
        if (stderr) {
          reject(stderr);
        }
        resolve(stdout);
      },
    );
  });
};

export const downloadAndExtractSpringCli = async (
  dir: string,
  pathToExtract: string,
): Promise<string> => {
  const path = await import('path');
  const Downloader = require('nodejs-file-downloader');
  const fileName = 'spring-cli.zip';

  const downloader = new Downloader({
    url: SpringCLIConfig.downloadUrl,
    directory: dir,
    cloneFiles: false,
    fileName,
  });
  try {
    await downloader.download();
    console.debug('Downloaded. Extracting...');

    await extractZip(path.join(dir, fileName), pathToExtract);
    return pathToExtract;
  } catch (error) {
    console.error('Download failed', error);
    process.exit(1);
  }
};

export const extractZip = async (source: string, target: string) => {
  const extract = await import('extract-zip');
  const MAX_FILES = 10000;
  const MAX_SIZE = 1000000000; // 1 GB
  const THRESHOLD_RATIO = 10;

  try {
    let fileCount = 0;
    let totalSize = 0;

    await extract(source, {
      dir: target,
      onEntry: (entry: any) => {
        fileCount++;
        if (fileCount > MAX_FILES) {
          throw new Error('Reached max. number of files');
        }

        // The uncompressedSize comes from the zip headers,
        // so it might not be trustworthy.
        // Alternatively, calculate the size from the readStream.
        const entrySize = entry.uncompressedSize;
        totalSize += entrySize;
        if (totalSize > MAX_SIZE) {
          throw new Error('Reached max. size');
        }

        if (entry.compressedSize > 0) {
          const compressionRatio = entrySize / entry.compressedSize;
          if (compressionRatio > THRESHOLD_RATIO) {
            throw new Error('Reached max. compression ratio');
          }
        }
      },
    });
  } catch (e) {
    console.error(e);
    process.exit(2);
  }

  console.debug('Extraction completed');
};

export const retrieveSpringCliPath = (p: string) => {
  const glob = require('glob');
  const path = require('path');

  return path.resolve(glob.sync(p)[0]);
};

export const isSpringBootFolder = (p: string): boolean => {
  const fs = require('fs');
  const path = require('path');

  return (
    fs.existsSync(path.join(p, 'src')) &&
    (fs.existsSync(path.join(p, 'pom.xml')) ||
      fs.existsSync(path.join(p, 'build.gradle')))
  );
};
