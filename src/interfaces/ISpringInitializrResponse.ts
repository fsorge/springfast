import IDependencies from './IDependencies';

export default interface ISpringInitializrResponse {
  dependencies: IDependencies;
}
