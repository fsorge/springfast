export interface IPomDependencySingle {
  groupId: string;
  artifactId: string;
}

export interface IPomDependency {
  dependency: IPomDependencySingle[];
}

export interface IPomProject {
  groupId: string;
  artifactId: string;
  version: string;
  name: string;
  description: string;
  dependencies: IPomDependency;
}

export default interface IPom {
  project: IPomProject;
}
