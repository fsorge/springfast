export default interface IField {
  name: string;
  columnName: string;
  type: string;
  isNullable: boolean;
  isPK: boolean;
  hasGeneratedValue: boolean;
}
