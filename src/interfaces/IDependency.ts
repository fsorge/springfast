export interface ILink {
  href: string;
  title: string;
}

export interface IDependencyLinks {
  reference: ILink;
}

export default interface IDependency {
  id: string;
  name: string;
  description: string;
  versionRage: string;
  _links: IDependencyLinks;
}
