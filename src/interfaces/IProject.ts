import IDependency from './IDependency';

export default interface IPRoject {
  package: string;
  artifact: string;
  name: string;
  build: 'maven' | 'gradle';
  dependencies: IDependency[];
  description: string;
}
