import IDependency from './IDependency';

export default interface IDependencyGroup {
  name: string;
  values: IDependency[];
}
