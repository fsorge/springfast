import IDependencyGroup from './IDependencyGroup';

export default interface IDependencies {
  type: string;
  values: IDependencyGroup[];
}
