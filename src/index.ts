#!/usr/bin/env node

// Reads SpringFast's package.json for app version
const packageJson = require('../package.json');

import {
  execSpring,
  downloadAndExtractSpringCli,
  setSpringCliCmd,
  retrieveSpringCliPath,
  isSpringBootFolder,
} from './common';

// main
// Application header
console.log('   _____            _             ______           __     ');
console.log('  / ___/____  _____(_)___  ____ _/ ____/___ ______/ /_    ');
console.log('  \\__ \\/ __ \\/ ___/ / __ \\/ __ `/ /_  / __ `/ ___/ __/    ');
console.log(' ___/ / /_/ / /  / / / / / /_/ / __/ / /_/ (__  ) /_      ');
console.log('/____/ .___/_/  /_/_/ /_/\\__, /_/    \\__,_/____/\\__/      ');
console.log('    /_/                 /____/                            ');
console.log();
// Prints SpringFast version
console.log(`Version: ${packageJson.version}`);
console.log();

// Verify is Spring CLI is installed
execSpring('--version')
  // If it's installed, launches the generator or project configuration wizard
  .then(launchGeneratorOrProject)
  // Othwerise
  .catch(async () => {
    // Dynamically imports needed libraries
    const os = await import('os');
    const path = await import('path');
    const fs = await import('fs');

    // Defines the path where to extract SpringCLI (defaults to user home)
    const pathToExtract = path.join(os.homedir(), '.springfast', 'spring-cli');

    /*
    If the path does not exist, we download and extract SpringCLI
    (if it exists, we assume SpringCLI is installed and valid)
    */
    if (!fs.existsSync(pathToExtract)) {
      console.log(`Spring CLI not found. Downloading...`);
      await downloadAndExtractSpringCli(os.tmpdir(), pathToExtract);
    }

    // Because SpringCLI is not in the %path%, we set it temporarly
    setSpringCliCmd(
      // Dynamically retrieves SpringCLI executable path
      retrieveSpringCliPath(path.join(pathToExtract, '*', 'bin', 'spring')),
    );

    // We make sure that now SpringCLI is correctly installed and configured
    execSpring('--version')
      .then(launchGeneratorOrProject)
      .catch(async (e) => {
        // If it is still missing, we throw an error and exits the application
        console.error(e);
        process.exit(3);
      });
  });

/**
 * Decides if it should generate a new project or manage an existing one
 * @param stdout we need the stdout from previous CLi command to print
 * SpringCli version
 */
async function launchGeneratorOrProject(stdout: string) {
  // Get current working directory
  const path = process.cwd();

  // If it's not a Spring Boot project folder
  if (!isSpringBootFolder(path)) {
    // We load the generator module and launch it
    const { generator } = await import('./generator');
    await generator(stdout);
  } else {
    // We load the manager module and launch it
    const { project } = await import('./project');
    await project(path);
  }

  // When the modules have finished, it correctly exits the app
  process.exit(0);
}
