import { SpringCLIConfig } from './config/SpringCLI';
import { entry as Basic } from './generatorSteps/Basic';
import { entry as Dependencies } from './generatorSteps/Dependencies';
import { entry as Generate } from './generatorSteps/Generate';
import IPRoject from './interfaces/IProject';

let projectConfig: IPRoject = {
  package: 'com.example',
  artifact: 'demo',
  name: 'demo',
  build: 'maven',
  dependencies: [],
  description: '',
};

export const generator = async (stdout: string) => {
  console.log(
    `Using ${stdout.replace(/\r?\n|\r/g, '')} ${
      SpringCLIConfig.springCliPath === 'spring'
        ? 'from your path'
        : `located at ${SpringCLIConfig.springCliPath}`
    }`,
  ); // Print installed version and path

  // Ask package, name, description, build system
  projectConfig = await Basic(projectConfig);

  // Prints a list with dependencies that can be added to the project
  projectConfig = await Dependencies(projectConfig);

  // Review and generate project
  projectConfig = await Generate(projectConfig);
};
