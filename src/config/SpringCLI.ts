export const SpringCLIConfig = {
  downloadUrl:
    'https://repo.spring.io/release/org/springframework/boot/spring-boot-cli/2.5.0/spring-boot-cli-2.5.0-bin.zip',
  serviceUrl: 'https://start.spring.io',
  springCliPath: 'spring',
};

export default SpringCLIConfig;
