import prompts = require('prompts');

import { SpringCLIConfig } from '../config/SpringCLI';
import { escapeExec, exitOnAbort, execAsync } from '../common';

import IPRoject from '../interfaces/IProject';

let projectConfig: IPRoject;

/**
 * Entry point for project generation step
 * @param p project configuration
 * @returns the same project configuration
 */
export const entry = async (p: IPRoject): Promise<IPRoject> => {
  projectConfig = p;

  console.log();
  console.log("It's time to generate your project");

  // We build the init command
  let command = `init -g="${escapeExec(
    projectConfig.package,
  )}" -n="${escapeExec(projectConfig.name)}" --build="${escapeExec(
    projectConfig.build,
  )}" `;

  command += `--description="${escapeExec(projectConfig.description)}" `;

  // With dependencies (if selected)
  if (projectConfig.dependencies.length > 0) {
    command += `--dependencies="${escapeExec(
      projectConfig.dependencies.map((d) => d.id).join(','),
    )}" `;
  }

  command += `${projectConfig.name}`;

  // We show it to the user
  console.log("We're about to execute this command:");
  console.log(`spring ${command}`);
  console.log();

  const { accept } = await prompts({
    type: 'confirm',
    name: 'accept',
    initial: false,
    message: `Shall we start?`,
    onState: exitOnAbort,
  });

  // If user refuses
  if (!accept) {
    // We exit the application
    process.exit(4);
  }

  // We dynamically load "path" module
  const path = await import('path');

  const filePath = path.join(process.cwd(), projectConfig.name);

  console.log('Generating...');

  try {
    // We call SpringCLI for project generation
    await execAsync(SpringCLIConfig.springCliPath, command);
    console.log(`Project successfully generated at ${filePath}`);
  } catch (e) {
    console.error('An error occurred generating your Spring project');
    console.error(e);
    process.exit(7);
  }

  return projectConfig;
};
