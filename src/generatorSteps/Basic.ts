import prompts = require('prompts');

import { exitOnAbort } from '../common';
import IPRoject from '../interfaces/IProject';

let projectConfig: IPRoject;

/**
 * Entry point for Basic project generation configurations
 * @param p project configuration (should have default values at this stage)
 * @returns defined project configuration
 */
export const entry = async (p: IPRoject): Promise<IPRoject> => {
  projectConfig = p;

  let accept = false;
  do {
    console.log();

    projectConfig.package = await askProjectPackage();
    projectConfig.name = await askProjectName();
    projectConfig.description = await askProjectDescription();
    projectConfig.build = await askProjectBuild();

    const response = await prompts({
      type: 'confirm',
      name: 'accept',
      initial: false,
      message: `The project will have the following base properties:
    Package: ${projectConfig.package}
    Name: ${projectConfig.name}
    Description: ${
      projectConfig.description.length > 0 ? projectConfig.description : 'Empty'
    }
    Build: ${projectConfig.build}

    FQN: ${projectConfig.package}.${projectConfig.name}

Is it ok?`,
    });
    accept = response.accept;
  } while (!accept);

  return projectConfig;
};

/**
 * Asks for project package
 * @returns package in dot notation
 */
const askProjectPackage = async (): Promise<string> => {
  const { value } = await prompts({
    type: 'text',
    name: 'value',
    message: 'Package:',
    initial: projectConfig.package,
    onState: exitOnAbort,
    validate: (v: string) => {
      if (/\s/g.test(v)) {
        return 'Project package cannot contain white spaces';
      } else {
        return true;
      }
    },
    format: (val) => val.toLowerCase(),
  });

  return value;
};

/**
 * Asks for project name
 * @returns project name
 */
const askProjectName = async (): Promise<string> => {
  const { value } = await prompts({
    type: 'text',
    name: 'value',
    message: 'Name:',
    initial: projectConfig.name,
    onState: exitOnAbort,
    validate: (v: string) => {
      if (/\s/g.test(v)) {
        return 'Project name cannot contain white spaces';
      } else {
        return true;
      }
    },
    format: (val) => val.toLowerCase(),
  });

  return value;
};

/**
 * Asks for project description
 * @returns project description
 */
const askProjectDescription = async (): Promise<string> => {
  const { value } = await prompts({
    type: 'text',
    name: 'value',
    initial: projectConfig.description,
    message: 'Description:',
    onState: exitOnAbort,
  });

  return value;
};

/**
 * Asks for project build system
 * @returns chosen build system
 */
const askProjectBuild = async (): Promise<'maven' | 'gradle'> => {
  const { value } = await prompts({
    type: 'select',
    name: 'value',
    message: 'Build',
    onState: exitOnAbort,
    choices: [
      { title: 'Maven', value: 'maven' },
      { title: 'Gradle', value: 'gradle' },
    ],
    initial: projectConfig.build === 'maven' ? 0 : 1,
  });

  return value;
};
