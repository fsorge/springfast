import prompts = require('prompts');
import axios, { AxiosResponse } from 'axios';

import { exitOnAbort } from '../common';
import { SpringCLIConfig } from '../config/SpringCLI';
import IPRoject from '../interfaces/IProject';
import ISpringInitializrResponse from '../interfaces/ISpringInitializrResponse';
import IDependency from '../interfaces/IDependency';
import IDependencyGroup from '../interfaces/IDependencyGroup';

let projectConfig: IPRoject;
let dependencies: IDependencyGroup[];
const selectedDependencies: IDependency[] = [];

/**
 * Entry point for Dependencies project generation configuration
 * @param p project configuration
 * @returns defined project configuration with dependencies
 */
export const entry = async (p: IPRoject): Promise<IPRoject> => {
  projectConfig = p;

  console.log();
  console.log('Retrieving dependencies...');

  // Get all available dependencies from Spring Initializr (or another service)
  const resp: AxiosResponse<ISpringInitializrResponse> = await axios.get(
    SpringCLIConfig.serviceUrl,
  );

  dependencies = resp.data.dependencies.values;

  let accept = false;
  do {
    // Always print all selected dependencies
    console.log(
      `Selected dependencies: ${
        selectedDependencies.length === 0
          ? 'None'
          : selectedDependencies.map((d) => d.name).join(', ')
      }`,
    );

    // Asks in which category the app should dive the user in
    const { category } = await prompts({
      type: 'select',
      name: 'category',
      message: 'Choose a category or move on',
      onState: exitOnAbort,
      choices: [
        { title: 'Exit', value: 'exit' },
        ...dependencies.map((v) => ({ title: v.name, value: v.name })),
      ],
    });

    accept = category === 'exit';

    if (!accept) {
      await dependencySelection(category);
    }
  } while (!accept);

  return {
    ...projectConfig,
    dependencies: selectedDependencies,
  };
};

/**
 * Prints a menu where users can select all the dependencies they want
 * @param category the category for which we want to list dependencies
 */
const dependencySelection = async (category: string) => {
  /**
   * Filters out dependencies that do not belong to the chosen category
   * and maps dependencies to the SpringFast app dependencies data structure
   */
  const availableDeps = dependencies
    .filter((v) => v.name === category)[0]
    .values.map((v) => ({
      title: v.name,
      value: v,
      selected: selectedDependencies.includes(v),
    }));

  const { selDeps } = await prompts({
    type: 'multiselect',
    name: 'selDeps',
    message: `Select dependencies`,
    choices: availableDeps,
    onState: exitOnAbort,
  });

  // Finds removed dependencies
  const removedDeps = availableDeps.filter(
    (d) => d.selected && !selDeps.includes(d.value),
  );

  // We remove unselected dependencies
  for (const dep of removedDeps) {
    selectedDependencies.splice(
      selectedDependencies.findIndex((d) => d === dep.value),
      1,
    );
  }

  // We add selected dependencies that weren't in the selected list yet
  for (const dep of selDeps) {
    if (!selectedDependencies.includes(dep)) {
      selectedDependencies.push(dep);
    }
  }
};
